SpringBootWithVaadin

Vaadin Framework, Spring, Spring Security, Spring Boot, Maven, JPA, JBoss - WildFly

In the first section I am going to consider Maven, one of the most popular build frameworks out there. 
The second chapter is about Java Persistence API and database related problems. 
The third chapter will cover Spring as far as the XML based configuration is concerned, then i will use the new way: the so-called Spring Boot. 
Finally, i am going to implement an application in which i am going to use all the technologies i have discussed.